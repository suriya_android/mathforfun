package com.suriya.mathforfun

import android.graphics.Color
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private var num1 = 0
    private var num2 = 0
    private var correct:Int = 0
    private var incorrect:Int = 0
    private var summation = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        run()
    }

    private fun run(){
        randPositionAnswer()
        randPositionIncorrect()
        displayDefault()
        checkAnswer()

    }
    private fun displayDefault(){
        quest_num_1?.text = num1.toString()
        quest_num_2?.text = num2.toString()
        txt_correct?.text = "ถูก : $correct"
        txt_incorrect?.text = "ผิด : $incorrect"
    }
    private fun randPositionAnswer(){
        num1 = Random.nextInt(1,10)
        num2 = Random.nextInt(1,10)
        summation = num1 + num2
        when(Random.nextInt(1,3)){
            1 -> btn_ans_1?.text = summation.toString()
            2 -> btn_ans_2?.text = summation.toString()
            3 -> btn_ans_3?.text = summation.toString()
        }
    }
    private fun randPositionIncorrect(){
            if(btn_ans_1?.text != summation.toString()){
                btn_ans_1?.text = (summation +2).toString()
            }
            if(btn_ans_2?.text != summation.toString()){
                btn_ans_2?.text = (summation +1).toString()
            }
            if(btn_ans_3?.text != summation.toString()){
                btn_ans_3?.text = (summation -1).toString()
            }
    }
    private fun checkAnswer(){
        btnOnclick(btn_ans_1)
        btnOnclick(btn_ans_2)
        btnOnclick(btn_ans_3)
    }
    private  fun btnOnclick(btn: Button){
        btn?.setOnClickListener {
            if(btn?.text == summation.toString()){
                txt_result?.text = "ถูกต้อง"
                correct ++
                btn.setBackgroundColor(Color.GREEN)
            }else{
                txt_result?.text = "ไม่ถูกต้อง"
                incorrect ++

                btn.setBackgroundColor(Color.RED)
            }
            Handler().postDelayed({
                clearButtonColor(btn_ans_1,btn_ans_2,btn_ans_3)
                run()
            },250)
        }
    }
    private fun clearButtonColor(btn1:Button,btn2:Button,btn3: Button){
        btn1.setBackgroundColor(Color.GRAY)
        btn2.setBackgroundColor(Color.GRAY)
        btn3.setBackgroundColor(Color.GRAY)
//        YAY
//        Completed Design
    }
}
